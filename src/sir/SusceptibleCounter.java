package sir;

import repast.simphony.data2.AggregateDataSource;

public class SusceptibleCounter implements AggregateDataSource {

	private final String id = "SusceptibleCounter";

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Class<?> getDataType() {
		return Integer.class;
	}

	@Override
	public Class<?> getSourceType() {
		return Agent.class;
	}

	@Override
	public Object get(Iterable<?> objs, int size) {
		int result = 0;
		for (Object obj : objs) {
			Agent agent = (Agent) obj;
			if (agent.getInfectionState()==InfectionState.SUSCEPTIBLE) {
				result++;
			}
		}

		return new Integer(result);
	}

	@Override
	public void reset() {
		// empty
	}

}
