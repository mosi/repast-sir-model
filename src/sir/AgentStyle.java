package sir;

import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;

public class AgentStyle extends DefaultStyleOGL2D {

	@Override
	public Color getColor(Object o) {

		if (o instanceof Agent) {
			Agent agent = (Agent) o;
			switch (agent.getInfectionState()) {
			case INFECTIOUS:
				return Color.red;
			case SUSCEPTIBLE:
				return Color.blue;
			case RECOVERED:
				return Color.green;
			}
		}

		return super.getColor(o);
	}
}
