package sir;

import repast.simphony.data2.AggregateDataSource;

public class InfectiousCounter implements AggregateDataSource {

	private final String id = "InfectiousCounter";

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Class<?> getDataType() {
		return Integer.class;
	}

	@Override
	public Class<?> getSourceType() {
		return Agent.class;
	}

	@Override
	public Object get(Iterable<?> objs, int size) {
		int result = 0;
		for (Object obj : objs) {
			Agent agent = (Agent) obj;
			if (agent.getInfectionState()==InfectionState.INFECTIOUS) {
				result++;
			}
		}

		return new Integer(result);
	}

	@Override
	public void reset() {
		// empty
	}

}
