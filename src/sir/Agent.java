package sir;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.graph.Network;

public class Agent {

	private final double infectionRate;
	private final double recoverRate;

	private InfectionState infectionState = InfectionState.SUSCEPTIBLE;
	private Network<Agent> network;
	private ISchedule schedule;
	private ISchedulableAction scheduledEvent = null;
	private final String id;

	public Agent(Network<Agent> network, ISchedule schedule, Integer id) {
		this.network = network;
		this.schedule = schedule;
		this.id = String.format("%3d", id);
		Parameters params = RunEnvironment.getInstance().getParameters();
		infectionRate = params.getDouble("infectionRate");
		recoverRate = params.getDouble("recoverRate");
	}

	public Agent(Network<Agent> network, ISchedule schedule,
			InfectionState infectionState, Integer id) {
		this(network, schedule, id);
		this.infectionState = infectionState;
	}

	public void rescheduleInfectionEventIfPresent() {
		log("has been informed");
		if (infectionState == InfectionState.SUSCEPTIBLE) {
			if (scheduledEvent != null) {
				// have scheduled infection
				schedule.removeAction(scheduledEvent);
				log("retracted infection event at "
						+ format(scheduledEvent.getNextTime()));
			}
			scheduleInfection();
		}
	}

	public void scheduleNextEvent() {

		if (infectionState == InfectionState.SUSCEPTIBLE) {
			scheduleInfection();
		} else if (infectionState == InfectionState.INFECTIOUS
				&& recoverRate > 0) {
			scheduleRecovery();
		} else {
			scheduledEvent = null;
		}

	}

	private void scheduleRecovery() {
		double currentTime = schedule.getTickCount();
		double waitingTime = RandomHelper.createExponential(recoverRate)
				.nextDouble();
		scheduledEvent = schedule.schedule(
				ScheduleParameters.createOneTime(currentTime + waitingTime),
				this, "recover");
		log("scheduled recovery at " + format(currentTime + waitingTime));
	}

	private void scheduleInfection() {
		double currentTime = schedule.getTickCount();
		double infectiousNeighbors = getInfectiousNeighbors();
		log("has " + infectiousNeighbors + " infectious neighbours");
		if (infectiousNeighbors == 0.0) {
			scheduledEvent = null;
		} else {
			double rate = infectionRate * infectiousNeighbors;
			double waitingTime = RandomHelper.createExponential(rate)
					.nextDouble();
			scheduledEvent = schedule
					.schedule(
							ScheduleParameters.createOneTime(currentTime
									+ waitingTime), this, "getInfected");
			log("scheduled infection at " + format(currentTime + waitingTime));
		}
	}

	private double getInfectiousNeighbors() {
		int infectedNeighbours = 0;
		for (Agent agent : network.getAdjacent(this)) {
			if (agent.getInfectionState() == InfectionState.INFECTIOUS) {
				infectedNeighbours++;
			}

		}
		return infectedNeighbours;
	}

	public InfectionState getInfectionState() {
		return infectionState;
	}

	public void getInfected() {
		this.infectionState = InfectionState.INFECTIOUS;
		log("got infected");
		scheduleRecovery();
		informNeighbours();
	}

	public void recover() {
		this.infectionState = InfectionState.RECOVERED;
		log("recovered");
		informNeighbours();
	}

	private void informNeighbours() {
		for (Agent agent : network.getAdjacent(this)) {
			log("informing a neighbour");
			agent.rescheduleInfectionEventIfPresent();
		}
	}

	private String format(double time) {
		return String.format("%7.3f", time);
	}

	private void log(String message) {
		String timeString = format(schedule.getTickCount());
		System.out.println(timeString + ": Agent " + id + " " + message);
	}

	public String getId() {
		return id;
	}
}
