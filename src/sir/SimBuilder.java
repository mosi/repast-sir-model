package sir;

import java.util.ArrayList;
import java.util.List;

import repast.simphony.context.Context;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.graph.Network;

public class SimBuilder implements ContextBuilder<Agent> {

	private List<Agent> agents;

	@Override
	public Context<Agent> build(Context<Agent> context) {

		ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
		agents = new ArrayList<>();

		NetworkBuilder<Agent> netBuilder = new NetworkBuilder<>("network",
				context, false);
		Network<Agent> network = netBuilder.buildNetwork();

		Parameters params = RunEnvironment.getInstance().getParameters();
		int agentCount = params.getInteger("totalAgents");

		int infectedAgentCount = params.getInteger("infectiousAgents");
		int minNeighbours = 1;
		int maxNeighbours = 3;

		for (int i = 1; i <= agentCount - infectedAgentCount; i++) {
			Agent agent = new Agent(network, schedule, i);
			context.add(agent);
			agents.add(agent);
		}

		for (int i = 1; i <= infectedAgentCount; i++) {
			Agent agent = new Agent(network, schedule,
					InfectionState.INFECTIOUS,
					(i + agentCount - infectedAgentCount));
			context.add(agent);
			agents.add(agent);
		}

		for (Agent agent : context.getObjects(Agent.class)) {
			int numNeighbours = RandomHelper.nextIntFromTo(minNeighbours,
					maxNeighbours);
			for (int j = 0; j < numNeighbours; j++) {
				network.addEdge(agent, context.getRandomObject());
			}
		}

		schedule.schedule(ScheduleParameters.createOneTime(0), this,
				"scheduleEvents");

		return context;
	}

	public void scheduleEvents() {

		for (Agent agent : agents) {
			agent.scheduleNextEvent();
		}
	}
}
